#!/bin/bash

URLEX=https://api.exchangeratesapi.io/latest

if [ $1 ]; then
	if [ $2 ]; then
		if [ $3 ]; then
			URLEX=$URLEX'?base='$1'&symbols='$2
			RETORNO=$(curl $URLEX | jq ".rates.$2")
			VALORFINAL=$(python -c "print $RETORNO*$3")
			echo Chamando $URLEX
			echo Resultado: $RETORNO
			echo CONVERSAO: $VALORFINAL
		else
			echo Número da parâmetros inválidos. Recebidos: 2. Esperados: 3.
		fi	
	else
		echo Número da parâmetros inválidos. Recebidos: 1. Esperados: 3.
	fi
else
	echo Número de parâmetros inválidos. Recebidos: 0. Esperados: 3.
fi
